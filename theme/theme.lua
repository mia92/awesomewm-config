-- Monokai color scheme

--{{{ Main
local awful = require("awful")
awful.util = require("awful.util")
local dpi = require("beautiful").xresources.apply_dpi

theme = {}

home          = os.getenv("HOME")
config        = awful.util.getdir("config")
shared        = "/usr/share/awesome"
if not awful.util.file_readable(shared .. "/icons/awesome16.png") then
    shared    = "/usr/share/local/awesome"
end
sharedicons   = shared .. "/icons"
sharedthemes  = shared .. "/themes"
themename     = "/theme"
themedir      = config .. "/theme"

wallpaper1    = themedir .. "/background.jpg"
wallpaper2    = themedir .. "/background.png"
wpscript      = home .. "/.bin/.wallpaper"

if awful.util.file_readable(wallpaper1) then
  theme.wallpaper = wallpaper1
elseif awful.util.file_readable(wallpaper2) then
  theme.wallpaper = wallpaper2
elseif awful.util.file_readable(wpscript) then
  theme.wallpaper_cmd = wpscript
elseif awful.util.file_readable(wallpaper3) then
  theme.wallpaper = wallpaper3
else
  theme.wallpaper = wallpaper4
end
--}}}

-- {{{ Styles
theme.font_size = 12
theme.font_name = "DejaVu Sans Mono"
theme.font      = theme.font_name .. " " .. theme.font_size
theme.wibar_size = 40
theme.wibar_spacing = 24

-- {{{ Colors
theme.pallette = {
    '#cdcea3',
    '#2d2f34',
    '#7c7e53',
    '#8ea054',
    '#535844',
    '#5d7044',
    '#a5ae41',
    '#8fa1a0',
    '#5f6a6f',
    '#434454',
    '#c6a992'
}
local red = "#e52222"
local alt_red = "#e51818"
local green = "#a6e32d"
local alt_green = "#749f1f"
local orange = "#fc951e"
local alt_orange = "#fc5209"
local dark_blue = "#764cff"
local alt_dark_blue = "#4866ff"
local light_blue = "#67d9f0"
local alt_light_blue = "#1cd6f0"
local pink = "#fa2573"
local alt_pink = "#fa4594"
local text = "#e1e1da"
local bg = "#272822"
theme.fg_normal  = theme.pallette[1]
theme.fg_focus   = theme.pallette[7]
theme.fg_urgent  = theme.pallette[11]
theme.bg_normal  = theme.pallette[2]
theme.sidebar_bg_normal = theme.bg_normal
theme.bg_focus   = theme.pallette[10]
theme.bg_urgent  = theme.pallette[6]
theme.bg_systray = theme.pallette[6]

theme.volume_show_text = true
theme.volume_text_color = theme.fg_focus
theme.volume_fg_color = theme.pallette[1]
theme.volume_bg_color = theme.pallette[5]
theme.volume_mute_fg_color = theme.pallette[11]
theme.volume_mute_bg_color = theme.pallette[10]
-- }}}

-- {{{ Borders
theme.border_width  = dpi(4)
theme.border_width_focused  = dpi(4)
theme.border_normal = bg
theme.border_focus  = theme.bg_focus
theme.border_marked = orange
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = theme.border_focus
theme.titlebar_bg_normal = theme.border_normal
-- }}}

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- Example:
--theme.taglist_bg_focus = "#CC939300"
--theme.taglist_font = "FontAwesome 9.5"
-- }}}

-- {{{ Widgets
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.fg_widget        = "#AECF96"
--theme.fg_center_widget = "#88A175"
--theme.fg_end_widget    = "#FF5656"
--theme.bg_widget        = "#494B4F"
--theme.border_widget    = "#3F3F3F"
-- }}}

-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor]
-- }}}

-- {{{ Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = "32"
theme.menu_width  = "200"
-- }}}

-- {{{ Icons
-- {{{ Taglist
theme.taglist_fg_focus = theme.bg_normal
theme.taglist_bg_focus = theme.fg_focus
theme.taglist_fg_hover = theme.pallette[2]
theme.taglist_bg_hover = theme.pallette[6]
--theme.taglist_squares_sel   = themedir .. "/taglist/squarefz.png"
--theme.taglist_squares_unsel = themedir .. "/taglist/squarez.png"
--theme.taglist_squares_resize = "true"
-- }}}

-- {{{ Misc
theme.awesome_icon           = themedir .. "/awesome-icon.png"
theme.awesome_menu           = "  <b></b>   "
theme.menu_submenu_icon      = sharedthemes .. "/default/submenu.png"
theme.tasklist_floating_icon = sharedthemes .. "/default/tasklist/floatingw.png"
-- }}}

-- {{{ Layout
theme.layout_tile       = themedir .. "/layouts/tile.png"
theme.layout_tileleft   = themedir .. "/layouts/tileleft.png"
theme.layout_tilebottom = themedir .. "/layouts/tilebottom.png"
theme.layout_tiletop    = themedir .. "/layouts/tiletop.png"
theme.layout_fairv      = themedir .. "/layouts/fairv.png"
theme.layout_fairh      = themedir .. "/layouts/fairh.png"
theme.layout_spiral     = themedir .. "/layouts/spiral.png"
theme.layout_dwindle    = themedir .. "/layouts/dwindle.png"
theme.layout_max        = themedir .. "/layouts/max.png"
theme.layout_fullscreen = themedir .. "/layouts/fullscreen.png"
theme.layout_magnifier  = themedir .. "/layouts/magnifier.png"
theme.layout_floating   = themedir .. "/layouts/floating.png"
-- }}}

-- {{{ Titlebar
theme.titlebar_close_button_focus  = sharedthemes .. "/zenburn/titlebar/close_focus.png"
theme.titlebar_close_button_normal = sharedthemes .. "/zenburn/titlebar/close_normal.png"

theme.titlebar_ontop_button_focus_active  = sharedthemes .. "/zenburn/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = sharedthemes .. "/zenburn/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = sharedthemes .. "/zenburn/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = sharedthemes .. "/zenburn/titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = sharedthemes .. "/zenburn/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = sharedthemes .. "/zenburn/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = sharedthemes .. "/zenburn/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = sharedthemes .. "/zenburn/titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = sharedthemes .. "/zenburn/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = sharedthemes .. "/zenburn/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = sharedthemes .. "/zenburn/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = sharedthemes .. "/zenburn/titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = sharedthemes .. "/zenburn/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = sharedthemes .. "/zenburn/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = sharedthemes .. "/zenburn/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = sharedthemes .. "/zenburn/titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}

return theme
