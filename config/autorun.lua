return {
    {prg = "picom"},
    {prg = "roccateventhandler"},
--    {prg = "nm-applet"},
--    {prg = "pulseaudio", arg_string = "-D"},
    {prg = "~/.bin/potd"},
    {prg = "xsetroot", arg_string = "-cursor_name left_ptr"},
}
