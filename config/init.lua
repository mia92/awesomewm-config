local r = {}
--[[
    Mod4 = Super_L or Super_R
    Mod3 = CapsLock
    Mod2 = NumLock
    Mod1 = Alt_L or Alt_R
--]]
r.modkey = "Mod4"

-- This is used later as the default terminal and editor to run.
r.terminal = "konsole"
local editor = os.getenv("EDITOR") or "vim"
r.editor_cmd = r.terminal .. " -e " .. editor

local awful = require("awful")
-- Table of layouts to cover with awful.layout.inc, order matters.
r.layouts = {
    awful.layout.suit.fair,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.top,
    awful.layout.suit.max,
}
r.widgets = {
    network = {
        names = {
            "eno1",
        },
    },
    tasklist = {
        title_font = "DejaVu Sans Light 12",
        rmbmenu_width = 500,
    },
}
r.sidebar = {
    position = "left",
    width = 300,
    tasklist = { item_height = 50 },
    widgets = {
        top = {
            "large_clock",
            "calendar",
        },
        middle = {
            "tasklist",
        },
        bottom = {
            "cpu",
            "uptime",
            --"network",
        }
    },
}
return r
