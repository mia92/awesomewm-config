local awful = require("awful")
awful.mouse = require("awful.mouse")
local beautiful = require("beautiful")
local config = require("config")
local gears = require("gears")
local menu = require("menu")
local menubar = require("misc.menubar")
require("ui.desktop")

bindings = {}

-- {{{ Key bindings
local volume = require("ui.widgets.volume")
local keymap = require("ui.widgets.keymap")
local desktop = require("ui.desktop")
bindings.globalkeys = gears.table.join(
    awful.key({ config.modkey,           }, "Left", awful.tag.viewprev),
    awful.key({ config.modkey,           }, "Right", awful.tag.viewnext),
    awful.key({ config.modkey,           }, "Escape", awful.tag.history.restore),
    awful.key({ "Control", "Shift"       }, "Escape", function() awful.util.spawn('ksysguard') end),
    awful.key({ config.modkey,           }, "d", function() desktop:toggle(mouse.screen) end),
    awful.key({ config.modkey,           }, "j", function () awful.client.focus.byidx( 1); if client.focus then client.focus:raise() end end),
    awful.key({ config.modkey,           }, "k", function () awful.client.focus.byidx(-1); if client.focus then client.focus:raise() end end),
    awful.key({ config.modkey,           }, "w", function() menu:show{keygrabber=false} end),
    awful.key({ config.modkey,           }, "space", keymap.switch),
    -- Layout manipulation
    awful.key({ config.modkey, "Shift"   }, "j", function() awful.client.swap.byidx(  1)    end),
    awful.key({ config.modkey, "Shift"   }, "k", function() awful.client.swap.byidx( -1)    end),
    awful.key({ config.modkey, "Control" }, "j", function() awful.screen.focus_relative( 1) end),
    awful.key({ config.modkey, "Control" }, "k", function() awful.screen.focus_relative(-1) end),
    awful.key({ config.modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ config.modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),
    -- Standard program
    awful.key({ config.modkey,           }, "Return", function() awful.util.spawn(config.terminal) end),
    awful.key({ config.modkey, "Control" }, "r", awesome.restart),
    awful.key({ config.modkey, "Shift"   }, "q", awesome.quit),
    awful.key({ config.modkey,           }, "l", function() awful.tag.incmwfact( 0.05) end),
    awful.key({ config.modkey,           }, "h", function() awful.tag.incmwfact(-0.05) end),
    awful.key({ config.modkey, "Shift"   }, "h", function() awful.tag.incnmaster( 1) end),
    awful.key({ config.modkey, "Shift"   }, "l", function() awful.tag.incnmaster(-1) end),
    awful.key({ config.modkey, "Control" }, "h", function() awful.tag.incncol( 1) end),
    awful.key({ config.modkey, "Control" }, "l", function() awful.tag.incncol(-1) end),
    awful.key({ config.modkey,           }, "z", function() awful.layout.inc(config.layouts,  1) end),
    awful.key({ config.modkey, "Shift"   }, "z", function() awful.layout.inc(config.layouts, -1) end),
    awful.key({ config.modkey, "Control" }, "n", awful.client.restore),
    -- Menubar
    awful.key({ config.modkey }, "p", function() menubar.show() end),
    -- Volume control
    awful.key({ }, "XF86AudioRaiseVolume", volume.up),
    awful.key({ }, "XF86AudioLowerVolume", volume.down),
    awful.key({ }, "XF86AudioMute", volume.toggleMute)
)

function bindings:bind_global(key)
    self.globalkeys = gears.table.join(self.globalkeys, key)
    root.keys(self.globalkeys)
end

bindings.clientkeys = gears.table.join(
    awful.key({ config.modkey,           }, "f", function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ config.modkey, "Shift"   }, "f", function (c)
        awful.client.floating.toggle(c)
        if c.floating then
            awful.titlebar.hide(c)
            local clientX = screen[1].workarea.x
            local clientY = screen[1].workarea.y
            local clientWidth = 0
            -- look at http://www.rpm.org/api/4.4.2.2/llimits_8h-source.html
            local clientHeight = 2147483640
            for s = 1, screen.count() do
                clientHeight = math.min(clientHeight, screen[s].workarea.height)
                clientWidth = clientWidth + screen[s].workarea.width
            end
            local t = c:geometry({x = clientX, y = clientY, width = clientWidth, height = clientHeight})
        else
            --apply the rules to this client so he can return to the right tag if there is a rule for that.
            awful.rules.apply(c)
        end
        -- focus our client
        client.focus = c
    end),
    awful.key({ config.modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ config.modkey, "Control" }, "space",  awful.client.floating.toggle),
    awful.key({ config.modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ config.modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ config.modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ config.modkey, "Shift"   }, "t",      awful.titlebar.toggle),
    awful.key({ config.modkey, "Shift"   }, "s",      function (c) c.sticky = not c.sticky          end),
    awful.key({ config.modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ config.modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
            if c.maximized_horizontal or c.maximized_vertical then
                c.border_width = 0
            else
                c.border_width = beautiful.border_width_focused
            end
        end),
    awful.key({ config.modkey, "Shift"   }, "m", function(c) c.maximized_horizontal = not c.maximized_horizontal end),
    awful.key({ config.modkey, "Control" }, "m", function(c) c.maximized_vertical   = not c.maximized_vertical end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    bindings.globalkeys = gears.table.join(bindings.globalkeys,
        -- View tag only.
        awful.key({ config.modkey }, "#" .. i + 9, function ()
            local tag = mouse.screen.tags[i]
            if tag then tag:view_only() end
        end),
        -- Toggle tag.
        awful.key({ config.modkey, "Control" }, "#" .. i + 9, function ()
            local tag = mouse.screen.tags[i]
            if tag then awful.tag.viewtoggle(tag) end
        end),
        -- Move client to tag.
        awful.key({ config.modkey, "Shift" }, "#" .. i + 9, function ()
            local c = client.focus
            if c == nil then return end
            local tag = c.screen.tags[i]
            if tag then c:move_to_tag(tag) end
        end),
        -- Toggle tag.
        awful.key({ config.modkey, "Control", "Shift" }, "#" .. i + 9, function ()
            local c = client.focus
            if c == nil then return end
            local tag = c.screen.tags[i]
            if tag then c:toggle_tag(tag) end
        end))
end

bindings.clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ config.modkey }, 1, awful.mouse.client.move),
    awful.button({ config.modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(bindings.globalkeys)
-- }}}
return bindings
