local beautiful = require("beautiful")
local config = require("config")
local awful = require("awful")

menu_items = {
    { symbolic_icon = "", text = "Dolphin", cmd = "dolphin" },
    { symbolic_icon = "", text = "Steam", cmd = "env STEAM_RUNTIME_PREFER_HOST_LIBRARIES=1 force_s3tc_enable=true steam" },
    { symbolic_icon = "", text = "Firefox", cmd = "firefox" },
    { symbolic_icon = "", text = "Gwenview", cmd = "gwenview" },
    { symbolic_icon = "", text = "VLC", cmd = "vlc" },
    { symbolic_icon = "", text = "Kontact", cmd = "kontact" },
    { symbolic_icon = "", text = "Skype", cmd = "env PULSE_LATENCY_MSEC=30 skypeforlinux" },
    { symbolic_icon = "", text = "LibreOffice", cmd = "libreoffice" },
    { symbolic_icon = "", text = "Settings", cmd = {
        { symbolic_icon = "", text = "Configure KDE", cmd = "systemsettings5" },
        { symbolic_icon = "", text = "YaST", cmd = "kdesu yast2" },
    }},
    { symbolic_icon = "", text = "Awesome", cmd = {
        { symbolic_icon = "", text = "Manual", cmd = config.terminal .. " -e man awesome" },
        { symbolic_icon = "", text = "Edit config", cmd = config.editor_cmd .. " " .. awesome.conffile },
        { symbolic_icon = "", text = "Powersave off", cmd = "xset s 600" },
        { symbolic_icon = "", text = "Powersave on", cmd = "xset s 1" },
        { symbolic_icon = "", text = "Restart", cmd = awesome.restart },
        { symbolic_icon = "", text = "Quit", cmd = function() awesome.quit() end },
    }},
    { symbolic_icon = "", text = "Open terminal", cmd = config.terminal },
}
return awful.menu.new{
    items = menu_items,
    theme = {
        iconfont = "FontAwesome 14",
    }
}
