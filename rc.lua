print("[awesome] rc.lua:1: " .. os.date())
local autofocus = require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- enable luajit
pcall(function() jit.on() end)
require("misc/error_handler")
local config = require("config")
require("autorun")
require("misc.beautiful")
require("ui")
require("signals")
