local naughty = require("naughty")
local awful = require("awful")
awful.mouse = require("awful.mouse")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")

-- Signal function to execute when a new client appears.
client.connect_signal(
    "manage",
    function(c, startup)
        -- Enable sloppy focus
        c:connect_signal(
            "mouse::enter",
            function(c)
                if awful.client.focus.filter(c) then
                    client.focus = c
                end
            end)
        if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)
        -- Put windows in a smart way, only if they does not set an initial
        -- position.
            if not c.size_hints.user_position
            and not c.size_hints.program_position then
                awful.placement.no_overlap(c)
                awful.placement.no_offscreen(c)
            end
        end
        local titlebars_enabled = true
        if titlebars_enabled then
            -- buttons for the titlebar
            local buttons = gears.table.join(
                awful.button(
                    { },
                    1,
                    function()
                        client.focus = c
                        c:raise()
                        awful.mouse.client.move(c)
                    end),
                awful.button(
                    { },
                    3,
                    function()
                        client.focus = c
                        c:raise()
                        awful.mouse.client.resize(c)
                    end))
            local layout = wibox.widget{
                layout = wibox.layout.align.horizontal,
                {
                    layout = wibox.layout.fixed.horizontal,
                    buttons = buttons,
                    awful.titlebar.widget.iconwidget(c),
                },
                {
                    layout = wibox.layout.flex.horizontal,
                    buttons = buttons,
                    awful.titlebar.widget.titlewidget(c),
                },
                {
                    layout = wibox.layout.fixed.horizontal,
                    awful.titlebar.widget.floatingbutton(c),
                    awful.titlebar.widget.maximizedbutton(c),
                    awful.titlebar.widget.stickybutton(c),
                    awful.titlebar.widget.ontopbutton(c),
                    awful.titlebar.widget.closebutton(c),
                },
            }
            layout.second.children[1]:set_align("center")

            awful.titlebar(c):set_widget(layout)
            if not c.floating then
                awful.titlebar.hide(c)
            end
        end
    end)
client.connect_signal(
    "focus",
    function(c)
        if c.maximized_horizontal or c.maximized_vertical then
            c.border_width = 0
        else
            c.border_width = beautiful.border_width_focused
        end
        c.border_color = beautiful.border_focus
    end)
client.connect_signal(
    "unfocus",
    function(c)
        if c.maximized_horizontal or c.maximized_vertical then
            c.border_width = 0
        else
            c.border_width = beautiful.border_width
        end
        c.border_color = beautiful.border_normal
    end)
client.connect_signal(
    "property::floating",
    function (c)
        -- Remove the titlebar if fullscreen
        if c.fullscreen and awful.titlebar.remove then
            awful.titlebar.remove(c)
        else
            -- Add title bar for floating apps
            if c.floating then
                awful.titlebar.show(c)
                -- Remove title bar, if it's not floating
            else
                awful.titlebar.hide(c)
            end
        end
    end)

awful.screen.connect_for_each_screen(function(s)
    s:connect_signal("arrange", function()
        local clients = awful.client.visible(s)
        local layout  = awful.layout.getname(awful.layout.get(s))

        if #clients > 0 then -- Fine grained borders and floaters control
            for _, c in pairs(clients) do -- Floaters always have borders
                if c.floating or layout == "floating" then
                    c.border_width = beautiful.border_width

                elseif #clients == 1 or layout == "max" then
                    -- No borders with only one visible client
                    c.border_width = 0
                else
                    c.border_width = beautiful.border_width
                end
            end
        end
    end)
end)
