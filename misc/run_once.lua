local awful = require("awful")
local naughty = require("naughty")
function run_once(args)
    local args = args or {}
    if not args.prg then
        do return nil end
    end
    local pname = (function() if not args.pname then return args.prg else return args.pname end end)()
    awful.spawn.easy_async_with_shell(
        "bash -c 'pgrep -f -u $USER -x \""..pname.."\"'",
        function(stdout, stderr, reason, exitcode)
            if stdout == "" or stdout == nil then
                if not args.arg_string then
                    awful.spawn{args.prg}
                else
                    awful.spawn{args.prg .. " " .. args.arg_string}
                end
            end
        end)
end
