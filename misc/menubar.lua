local menubar = require("menubar")
local config = require("config")
menubar.utils.terminal = config.terminal -- Set the terminal for applications that require it
menubar.geometry = {
    height = 48,
    width = screen[1].workarea.width,
    x = 0,
    -- y = 0, -- screen[1].workarea.height - 48,
}
return menubar
