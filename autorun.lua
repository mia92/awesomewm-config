require("misc.run_once")
local runlist = require("config.autorun")
for x, cmd in pairs(runlist) do
    run_once(cmd)
end
