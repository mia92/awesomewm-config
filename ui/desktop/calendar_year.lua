local awful = require("awful")
local config = require("config")
local base = require('wibox.widget.base')
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")
local naughty = require("naughty")

local calendar_year = { mt = {} }
setmetatable(calendar_year, calendar_year.mt)

local function rounded_shape(radius)
    return function(cr, width, height)
        gears.shape.rounded_rect(cr, width, height, radius)
    end
end
local styles = {
    year = {
        bg_color     = beautiful.bg_normal .. '00' ,
    },
    yearheader = {
        fg_color = beautiful.taglist_fg_normal,
        bg_color = beautiful.bg_normal .. '00',
        markup   = function(t) return '<b>' .. t .. '</b>' end,
    },
    month   = {
        padding      = 5,
        bg_color     = beautiful.pallette[10],
        border_width = 1,
        border_color = beautiful.pallette[7],
        shape        = rounded_shape(5),
    },
    normal = {
        shape    = rounded_shape(5),
    },
    focus = {
        fg_color = beautiful.fg_urgent,
        bg_color = beautiful.bg_urgent,
        shape    = rounded_shape(5, true),
    },
    header = {
        fg_color = beautiful.pallette[7],
        shape    = rounded_shape(10),
        markup   = function(t) return '<b>' .. t .. '</b>' end,
    },
    weeknumber = {
        fg_color = beautiful.pallette[7],
        bg_color = beautiful.pallette[9],
        shape    = rounded_shape(5),
    },
    weekday = {
        fg_color = beautiful.pallette[7],
        bg_color = beautiful.pallette[10],
        shape    = rounded_shape(5),
    },
}
local function decorate_cell(widget, flag, date)
    if (flag=='monthheader' and not styles.monthheader) or
        (flag=='yearheader' and not styles.yearheader) then
        flag = 'header'
    end
    local props = styles[flag] or {}
    if props.markup and widget.get_text and widget.set_markup then
        widget:set_markup(props.markup(widget:get_text()))
    end
    -- Change bg color for weekends
    local d = {year=date.year, month=(date.month or 1), day=(date.day or 1)}
    local weekday = tonumber(os.date('%w', os.time(d)))
    local default_bg = (weekday==0 or weekday==6) and beautiful.pallette[9] or beautiful.pallette[2]
    local ret = wibox.widget {
        {
            widget,
            margins = (props.padding or 2) + (props.border_width or 0),
            widget  = wibox.container.margin,
        },
        shape              = props.shape,
        shape_border_color = props.border_color or '#b9214f',
        shape_border_width = props.border_width or 0,
        fg                 = props.fg_color or beautiful.pallette[7],
        bg                 = props.bg_color or (flag == 'normal' and default_bg or beautiful.pallette[2]),
        widget             = wibox.container.background,
    }
    return ret
end

function calendar_year.new(screen)
    local r = wibox.widget {
        widget = wibox.container.background,
        bg = beautiful.bg_normal .. 'd0',
        fg = beautiful.fg_normal,
        shape = function(cr, w, h) return gears.shape.rounded_rect(cr, w, h, 24) end,
        shape_border_width = 4,
        shape_border_color = beautiful.bg_urgent,
        {
            widget = wibox.container.margin,
            margins = 18,
            {
                widget = wibox.widget.calendar.month,
                date = os.date('*t'),
                -- week_numbers = true,
                -- spacing = 2,
                font = 'DejaVu Sans Mono',
                fn_embed = decorate_cell,
            },
        },
    }
    r.timer = gears.timer{ timeout = 60, }

    function r:update()
        self.widget.widget.date = os.date('*t')
        -- self.date = os.date('*t')
    end

    r:update()
    r.timer:connect_signal("timeout", function() r:update() end)
    r.timer:start()

    return r
end

function calendar_year.mt:__call(...)
    return calendar_year.new(...)
end

return calendar_year
