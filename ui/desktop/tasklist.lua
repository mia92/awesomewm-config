local awful = require("awful")
local config = require("config")
local base = require('wibox.widget.base')
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")
local naughty = require("naughty")

local tasklist_buttons =
    gears.table.join(
        awful.button({ }, 1, function (c)
            if c == client.focus then return end
            c.minimized = false
            if not c:isvisible() then
                c:tags()[1]:view_only()
            end
            -- This will also un-minimize
            -- the client, if needed
            client.focus = c
            c:raise()
        end),
        awful.button({ }, 3, function ()
            -- TODO Swap out with 'focus/raise/minimise/close' menu.
            if instance then
                instance:hide()
                instance = nil
            else
                instance = awful.menu.clients(
                {theme = { width = config.widgets.tasklist.rmbmenu_width }})
            end
        end),
        awful.button({ }, 4, function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
        awful.button({ }, 5, function ()
            awful.client.focus.byidx(1)
            if client.focus then client.focus:raise() end
        end))

local tasklist = {}

function tasklist.new(screen)
    local r = wibox.widget {
        widget = wibox.container.background,
        bg = beautiful.bg_normal .. '80',
        fg = beautiful.fg_normal,
        shape = function(cr, w, h) return gears.shape.rounded_rect(cr, w, h, 24) end,
        shape_border_width = 4,
        shape_border_color = beautiful.bg_urgent,
        {
            widget = wibox.container.margin,
            id = 'tasklist_margin',
            margins = 18,
            awful.widget.tasklist {
                screen = screen,
                filter = awful.widget.tasklist.filter.alltags,
                buttons = tasklist_buttons,
                style = {
                    shape_border_width = 2,
                    shape_border_color = beautiful.pallette[10],
                    shape = function(cr, w, h) return gears.shape.rounded_rect(cr, w, h, 12) end,
                },
                layout = {
                    layout  = wibox.layout.flex.vertical,
                    max_widget_size = 64,
                    spacing = 10,
                    spacing_widget = {
                        widget = wibox.container.margin,
                        left = 64,
                        right = 64,
                        top = 3,
                        bottom = 3,
                        {
                            widget = wibox.container.place,
                            valign = 'center',
                            halign = 'center',
                            {
                                widget = wibox.widget.separator,
                                shape = gears.shape.rounded_bar,
                            },
                        },
                    },
                },
                widget_template = {
                    widget = wibox.container.background,
                    id = 'background_role',
                    {
                        widget = wibox.container.margin,
                        top = 10,
                        bottom = 10,
                        {
                            layout = wibox.layout.fixed.horizontal,
                            {
                                widget = wibox.container.margin,
                                id = 'icon_margin_role',
                                left = 32,
                                right = 32,
                                top = 8,
                                bottom = 8,
                                {
                                    widget = wibox.widget.imagebox,
                                    id = 'icon_role',
                                },
                            },
                            {
                                widget = wibox.widget.textbox,
                                id = 'text_role',
                                font = 'DejaVu Sans Light 24',
                            },
                        },
                    },
                },
            },
        },
    }

    return r
end

return tasklist
