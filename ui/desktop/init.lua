local awful = require("awful")
local beautiful = require("beautiful")
local common = require("awful.widget.common")
local config = require("config")
local naughty = require("naughty")
local wibox = require("wibox")

desktop = {}

function desktop:toggle(s)
    self[s].ontop = not self[s].ontop
end

--[[
  Desktop widget set:

  +--------------------+-------------------------+--------------------+
  |                    |                         |   NETWORK          |
  |                    |                         +--------------------+
  |                    |     ????                |   STORAGE          |
  |                    |                         +--------------------+
  |                    +-------------------------+   CPU/MEM          |
  |    TASKLIST        |                         +--------------------+
  |                    |     CLOCK               |                    |
  |                    |                         |                    |
  |                    +-------------------------+   ????             |
  |                    |                         |                    |
  |                    |     CALENDAR            |                    |
  |                    |                         |                    |
  +--------------------+-------------------------+--------------------+

--]]
function desktop.render(screen)
    r = wibox{
        screen = screen,
        x = beautiful.wibar_spacing / 2 + beautiful.wibar_size,
        y = beautiful.wibar_spacing / 2,
        width = screen.geometry.width - beautiful.wibar_spacing - beautiful.wibar_size,
        height = screen.geometry.height - beautiful.wibar_spacing,
        opacity = 100,
        ontop = false,
        visible = true,
        bg = beautiful.bg_normal .. '00',
    }

    r:setup {
        layout = wibox.layout.grid,
        homogeneous = true,
        expand = true,
        forced_num_cols = 7,
        forced_num_rows = 7,
        spacing = beautiful.wibar_spacing,
    }

    megaclock = require('ui.widgets.megaclock').new(screen)
    r.widget:add_widget_at(megaclock, 3, 3, 3, 3)
    tasklist = require('ui.desktop.tasklist').new(screen)
    r.widget:add_widget_at(tasklist, 1, 1, 7, 2)
    calendar_year = require('ui.desktop.calendar_year').new(screen)
    r.widget:add_widget_at(calendar_year, 6, 3, 2, 3)
    desktop[screen] = r
end

return desktop
