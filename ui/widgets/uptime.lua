local awful = require("awful")
local vicious = require("vicious")
local wibox = require("wibox")

local r = {}

r.widget = wibox.widget.textbox()
vicious.register(
    r.widget,
    vicious.widgets.uptime,
    function(widget, args)
        return ("<b>Uptime</b> %dd %dh %dmin"):
            format(args[1], args[2], args[3])
    end,
    5)

return r
