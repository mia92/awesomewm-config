local config = require("config")
local vicious = require("vicious")
local wibox = require("wibox")
local naughty = require("naughty")

local r = {}

r.widget = wibox.widget.textbox()
vicious.register(
    r.widget,
    vicious.widgets.net,
    function (widget, args)
        local file = io.popen(
            "ip link | sed 'n;d' | awk '{print $2}' | sed s/.$// | tail -n+2")
        local arr = {}
        for line in file:lines() do
            table.insert (arr, line);
        end
        file:close()
        ret = '<b>Network</b>\n'
        networks = config.widgets.network.names
        for i, net in ipairs(arr) do
            ret = ret .. '<b>    ' .. net .. '</b> '
                .. ('<span color="#CC9393">D: %6.1f'):format(args['{' .. net .. ' down_kb}']) .. '</span> '
                .. ('<span color="#7F9F7F">U: %6.1f'):format(args['{' .. net .. ' up_kb}']) .. '</span>\n'
        end
        return string.sub(ret, 1, -2)
    end,
    3)
return r
