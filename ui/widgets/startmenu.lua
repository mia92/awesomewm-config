local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local menu = require("menu")
local wibox = require("wibox")

r = wibox.widget {
    widget = wibox.container.background,
    bg = beautiful.bg_urgent,
    fg = beautiful.fg_normal,
    shape  = function(cr, width, height)
        return gears.shape.partially_rounded_rect(
        cr, width, height, false, false, true, false, 12)
    end,
    {
        widget = wibox.container.margin,
        left = (beautiful.wibar_size - beautiful.font_size) / 2,
        {
            widget = wibox.widget.textbox,
            markup = beautiful.awesome_menu,
        },
    },
}

r:buttons(gears.table.join(
    awful.button({ }, 1, function() menu:toggle() end),
    awful.button({ }, 3, function() require("ui.desktop"):toggle(mouse.screen) end)))
return r
