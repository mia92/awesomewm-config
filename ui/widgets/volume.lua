local awful = require("awful")
local beautiful = require("beautiful")
local gears = require('gears')
local wibox = require('wibox')

local height         = 100       -- height in pixels of progressbar
local step          = 0.05      -- stepsize for volume change (ranges from 0 to 1)
local color         = '#698f1e' -- foreground color of progessbar
local color_bg      = '#33450f' -- background color
local color_mute    = '#be2a15' -- foreground color when muted
local color_bg_mute = '#532a15' -- background color when muted
local mixer         = 'pavucontrol' -- mixer command
local text_color    = '#fff020' -- color of text
-- default colors overridden by Beautiful theme
color = beautiful.volume_fg_color or color
color_bg = beautiful.volume_bg_color or color_bg
color_mute = beautiful.volume_mute_fg_color or color_mute
color_bg_mute = beautiful.volume_mute_bg_color or color_bg_mute
text_color = beautiful.volume_text_color or text_color

local r = wibox.widget{
    layout = wibox.layout.align.vertical,
    {
        widget = wibox.container.margin,
        margins = 3,
        {
            widget = wibox.widget.imagebox,
            id = "vol_icon",
            resize = true,
        },
    },
    {
        layout = wibox.layout.stack,
        {
            widget = wibox.container.rotate,
            direction = 'east',
            {
                widget = wibox.widget.progressbar,
                id = "vol_progbar",
                max_value = 100,
                -- forced_height = height,
                forced_width = height,
                margins = {
                    top = 12,
                    bottom = 12,
                },
            },
        },
        {
            widget = wibox.container.rotate,
            direction = 'east',
            {
                widget = wibox.widget.slider,
                id = "vol_slider",
                forced_width = height,
                visible = true,
                bar_height = 0,
                handle_width = 0,
                bar_color = '#ff0000',
            },
        },
    },
    {
        widget = wibox.widget.textbox,
        id = "vol_text",
        align = "center",
        valign = "center",
        forced_width = 40,
    },
}
r.volume = 0
r:get_children_by_id("vol_slider")[1].value = r.volume
r.mute = false
r.icon_path = '/usr/share/icons/Arc/status/symbolic/'
r.timer = gears.timer({ timeout = 1.5 })

function r.setColor(mute)
    local new_color = mute and color_mute or color
    local new_color_bg = mute and color_bg_mute or color_bg
    r:get_children_by_id("vol_slider")[1].bar_color = new_color
    r:get_children_by_id("vol_slider")[1].bar_background_color = new_color_bg
    r:get_children_by_id("vol_progbar")[1].color = new_color
    r:get_children_by_id("vol_progbar")[1].background_color = new_color_bg
end

function r.setIcon(mute, vol)
    local widget = r:get_children_by_id("vol_icon")[1]
    local state = (mute or vol == 0.0) and 'muted'
    or vol < 0.33  and 'low'
    or vol < 0.67  and 'medium'
    or 'high'
    widget.image = r.icon_path .. 'audio-volume-' .. state .. '-symbolic.svg'
end

function r.setMixer(command)
    mixer = command
end

local function run(c)
    local p = io.popen(c)
    out = p:read("*a")
    p:close()
    return out
end

local function escaped(s)
    return s and string.gsub(string.gsub(s, "%-", "%%-"), "%.", "%%.") or 'esc'
end

function r.setVolume(vol)
    if default_sink == nil then return end
    vol = math.floor(math.min(math.max(vol, 0.0), 1.0) * 0x10000)
    run("pacmd set-sink-volume " .. default_sink .. " " .. string.format("0x%x", vol))
    r.update()
end


function r.toggleMute()
    if default_sink == nil then return end
    val = r.mute and "0" or "1"
    run("pacmd set-sink-mute " .. default_sink .. " " .. val)
    r.update()
end

local _update_slider

local function _update()
    local slider = r:get_children_by_id("vol_slider")[1]
    local progbar = r:get_children_by_id("vol_progbar")[1]
    slider:disconnect_signal("property::value", _update_slider)
    slider.value = r.volume * 100
    slider:connect_signal("property::value", _update_slider)
    progbar.value = r.volume * 100
    r.setColor(r.mute)
    r.setIcon(r.mute, r.volume)
    r:get_children_by_id("vol_text")[1]:set_markup('<span foreground="'..text_color..'">'..math.ceil(r.volume*100)..'%</span>')
end

_update_slider = function(w)
    local new_vol = math.floor(w.value / 5) / 20
    r.setVolume(new_vol)
    _update()
end

function r.up()
    r.setVolume(r.volume + step)
    _update()
end

function r.down()
    r.setVolume(r.volume - step)
    _update()
end


function r.launchMixer()
    awful.spawn.with_shell(mixer)
end

function r.update()
    local out = run("pacmd dump")

    default_sink = string.match(out, "set%-default%-sink ([^\n]+)")

    local vol = string.match(out, "set%-sink%-volume " .. escaped(default_sink) .. " (0x%x+)")
    if vol == nil then return end
    r.volume = tonumber(vol) / 0x10000

    local m = string.match(out, "set%-sink%-mute " .. escaped(default_sink) .. " (%a+)")
    if m == nil then return end
    r.mute = (m == "yes")
    _update()
end

r.timer:connect_signal("timeout", r.update)
r.timer:start()

-- register mouse button actions
r.children[1]:buttons(gears.table.join(
awful.button({ }, 1, r.toggleMute),
awful.button({ }, 3, r.launchMixer),
awful.button({ }, 4, r.up),
awful.button({ }, 5, r.down)))
r.children[2]:buttons(gears.table.join(
awful.button({ }, 3, r.launchMixer),
awful.button({ }, 4, r.up),
awful.button({ }, 5, r.down)))

-- initialize
r.update()

return r
