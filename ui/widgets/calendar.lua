local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")

local button_left = wibox.widget.textbox()
button_left:set_align("right")
button_left:set_markup("<span color='" .. beautiful.fg_focus ..  "'>  </span>")

local button_recenter = wibox.widget.textbox()
button_recenter:set_align("center")
button_recenter:set_markup("<span color='" .. beautiful.fg_focus .. "'>  </span>")

local button_right = wibox.widget.textbox()
button_right:set_align("left")
button_right:set_markup("<span color='" .. beautiful.fg_focus ..  "'>  </span>")

local cal = wibox.widget{
    widget = wibox.widget.calendar.month,
    font = "Inconsolata-dz for Powerline 11",
    week_numbers = true,
    date = os.date('*t'),
    current_date = os.date('*t')
}

local r = wibox.widget{
    widget = wibox.container.margin,
    bottom = 10,
    left = 10,
    right = 15,
    {
        layout = wibox.layout.fixed.vertical,
        {
            widget = wibox.container.margin,
            left = 32,
            right = 32,
            {
                layout = wibox.layout.align.horizontal,
                button_left,
                button_recenter,
                button_right,
            },
        },
        {
            layout = wibox.layout.align.horizontal,
            expand = 'outside',
            nil,
            cal,
            nil,
        },
    }
}

function cal:offset(offset)
    assert(type(offset) == "number", "function offset expects a number, got "
            .. type(offset))

    local sum = offset + self.date.month + self.date.year * 12 - 1
    self:set_date{
        day = self.date.day,
        month = sum % 12 + 1,
        year = math.floor(sum / 12)
    }
end

function cal:update()
    self.date.day =
        self.current_date.year == self.date.year and
        self.current_date.month == self.date.month and
        self.current_date.day or nil
    self:set_date{
        day = self.date.day,
        month = self.date.month,
        year = self.date.year,
    }
end

function cal:timer_kick()
    self.current_date = os.date('*t')
    self:update()
end

function cal:previousMonth()
    self:offset(-1)
    self:update()
end

function cal:previousYear()
    self:offset(-12)
    self:update()
end

function cal:nextMonth()
    self:offset(1)
    self:update()
end

function cal:nextYear()
    self:offset(12)
    self:update()
end

function cal:recenter()
    self:set_date(os.date('*t'))
    self:update()
end

button_left:buttons(gears.table.join(awful.button({}, 1, function()
    cal:previousMonth()
end)))
button_recenter:buttons(gears.table.join(awful.button({}, 1, function()
    cal:recenter()
end)))
button_right:buttons(gears.table.join(awful.button({}, 1, function()
    cal:nextMonth()
end)))
cal:buttons(gears.table.join(
    awful.button({}, 1, function() cal:nextYear() end),
    awful.button({}, 3, function() cal:previousYear() end),
    awful.button({}, 4, function() cal:previousMonth() end),
    awful.button({}, 5, function() cal:nextMonth() end)
))

cal:update()
r.timer = gears.timer({ timeout = 60 })
r.timer:connect_signal("timeout", function() cal:timer_kick() end)
r.timer:start()

return r
