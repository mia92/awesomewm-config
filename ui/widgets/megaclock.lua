local base = require('wibox.widget.base')
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")
local naughty = require("naughty")

local megaclock = {}

function megaclock.new(screen)
    local r = wibox.widget {
        widget = wibox.container.background,
        bg = beautiful.bg_normal .. '80',
        fg = beautiful.fg_normal,
        shape = function(cr, w, h) return gears.shape.rounded_rect(cr, w, h, 24) end,
        shape_border_width = 4,
        shape_border_color = beautiful.bg_urgent,
        {
            id = 'text_role',
            widget = wibox.widget.textbox,
            align = 'center',
            valign = 'center',
        },
    }
    r.timer = gears.timer{ timeout = 1 }
    function r:update()
        tb = self:get_children_by_id('text_role')[1]
        tb.font = 'DejaVu Sans Light ' .. (self.fontscale and self.fontscale.large or 24)
        tb.markup = os.date("%H:%M\n<span font='DejaVu Sans Light " .. (self.fontscale and self.fontscale.small or 12) .. "'>%A, %d %b %Y</span>")
    end

    function r:layout(context, width, height)
        assert(width > height)
        self.fontscale = {
            large = math.floor(width / 5.9),
            small = math.floor(width / 17.6),
        }
        return { base.place_widget_at(self.widget, 0, 0, width, height) }
    end

    r:update()
    r.timer:connect_signal("timeout", function() r:update() end)
    r.timer:start()

    return r
end

return megaclock
