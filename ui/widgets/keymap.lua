local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")
-- Keyboard map indicator and changer {{{
local r = {}
r.cmd = "setxkbmap"
r.keymaps = {
    { "us", "" , "US" },
    { "bg", "phonetic" , "BG" },
--    { "it", "", "IT"},
}
r.current = 1  -- us is our default layout
r.widget = wibox.widget {
    widget = wibox.container.margin,
    margins = 3,
    {
        layout = wibox.layout.fixed.vertical,
        {
            widget = wibox.widget.textbox,
            align = 'center',
            valign = 'center',
            markup = '<span font_desc="FontAwesome 5 Free 12">⌨</span>',
        },
        {
            widget = wibox.widget.textbox,
            id = 'text_role',
            align = 'center',
            valign = 'center',
            markup = '<b>' .. r.keymaps[r.current][3] .. '</b>',
        },
    },
}
r.switch = function ()
    r.current = r.current % #(r.keymaps) + 1
    local t = r.keymaps[r.current]
    r.widget:get_children_by_id('text_role')[1].markup = "<b>" .. t[3] .. "</b>"
    if t[2] == "" then
        os.execute( r.cmd .. " " .. t[1] .. ",us" )
    else
        os.execute( r.cmd .. " " .. t[1] .. "\\(" .. t[2] .. "\\),us" )
    end
end
r.widget:buttons(
    gears.table.join(awful.button({ }, 1, r.switch))
)
return r
