local awful = require("awful")
local vicious = require("vicious")
local wibox = require("wibox")

local r = {}

r.widget = wibox.widget.textbox()
vicious.register(
    r.widget,
    vicious.widgets.cpu,
    function(widget, args)
        local ret = "<b>CPU</b>\n"
        local file = io.popen("cat /proc/cpuinfo | grep processor | wc -l")
        local num_cpu = tonumber(file:read("*all"))
        local num_rows = math.ceil(num_cpu / 4)
        file:close()
        str = ("<b>CPU</b> %3d%%\n    "):format(args[1])
        for i = 0, num_rows - 1 do
            str = str .. "<b>" .. 4 * i + 1 .. "-"
                .. math.min(4 * i + 4, num_cpu) .."</b> "
            for j = 4 * i + 2, math.min(4 * i + 5, num_cpu + 1) do
                str = str .. ("%3d%% "):format(args[j])
            end
            str = str .. "\n    "
        end
        return string.sub(str, 1, -6)
    end,
    5)

return r
