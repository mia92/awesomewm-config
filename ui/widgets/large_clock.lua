local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")

local r = {
    widget = wibox.widget {
        widget = wibox.container.margin,
        top = 20,
        bottom = 20,
        left = 5,
        right = 5,
        {
            layout = wibox.layout.fixed.vertical,
            {
                id = 'time',
                widget = wibox.widget.textbox,
                font = 'DejaVu Sans Light 32',
                align = 'center',
                valign = 'center',
            },
            {
                id = 'date',
                widget = wibox.widget.textbox,
                font = 'DejaVu Sans Light 18',
                align = 'center',
                valign = 'center',
            },
        },
    },
}
r.timer = gears.timer{ timeout = 1 }
r.update = function(self)
    local time = self.widget:get_children_by_id('time')[1]
    time.markup = os.date("%H:%M:%S")
    local date = self.widget:get_children_by_id('date')[1]
    date.markup = os.date("%A, %d %b %Y")
end

r:update()
r.timer:connect_signal("timeout", function() r:update() end)
r.timer:start()

return r
