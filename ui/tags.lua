-- Tyrannical tag manager
local tyrannical = require("tyrannical")
local awful = require("awful")
local tagnames = require("tagnames")
-- {{{ Tyrannical Tags
tyrannical.settings.default_layout = awful.layout.suit.tile.left
tyrannical.settings.master_width_factor = 0.50
tyrannical.settings.no_focus_stealing_out = true

-- Setup some tags
tyrannical.tags = {
    {
        name = tagnames.term,
        init = true, -- Load the tag on startup
        exclusive = true, -- Refuse any other type of clients (by classes)
        screen = {1,2}, -- Create this tag on screen 1 and screen 2
        position = 1,
        layout = awful.layout.suit.fair,
        selected = true,
        class = {
            -- Accept the following classes, refuse everything else (because of
            -- "exclusive=true")
            "xterm",
            "XTerm",
            "konsole",
        },
        no_focus_stealing_out = true,
    },
    {
        name = tagnames.www,
        -- init = true,
        exclusive = true,
        position = 2,
        --screen = screen.count()>1 and 2 or 1,
        screen = {1,2},
        layout = awful.layout.suit.max, -- Use the max layout
        class = {
            "Firefox",
            "Rekonq",
            "konqueror",
        },
        no_focus_stealing_out = true,
        no_focus_stealing_in = true,
    },
    {
        name = tagnames.skype,
        init = true,
        position = 3,
        exclusive = true,
        screen = {1,2},
        layout = awful.layout.suit.tile.left,
        master_width_factor = 0.2,
        -- exec_once = {"skype"},
        class = {
            "Skype",
            "Ktp-contactlist",
            "Ktp-text-ui",
        },
        no_focus_stealing_out = true,
        no_focus_stealing_in = true,
    },
    {
        name = tagnames.mail,
        init = true,
        position = 4,
        exclusive = true,
        screen = {1,2},
        layout = awful.layout.suit.fair,
        -- exec_once = {"thunderbird"},
        class = {
            "Thunderbird",
            "Kmail",
            "Kontact",
        },
        no_focus_stealing_out = true,
        no_focus_stealing_in = true,
    },
    {
        name = tagnames.files,
        init = true,
        position = 5,
        exclusive = true,
        screen = {1,2},
        layout = awful.layout.suit.tile,
        class = {
            "Dolphin",
            "ark",
        },
        no_focus_stealing_out = true,
        no_focus_stealing_in = true,
    },
    {
        name = tagnames.code,
        init = false,
        position = 7,
        exclusive = true,
        screen = {1,2},
        -- Create a single instance of this tag on screen 1, but also show it
        -- on screen 2. The tag can be used on both screen, but only one at once
        -- clone_on = 2,
        layout = awful.layout.suit.max ,
        class ={
            "Kate",
            "KDevelop",
            "DDD",
        },
        no_focus_stealing_out = true,
        no_focus_stealing_in = true,
    },
    {
        name = tagnames.doc,
        -- This tag wont be created at startup, but will be when one of the
        -- client in the "class" section will start. It will be created on
        -- the client startup screen
        init = false,
        exclusive = true,
        layout = awful.layout.suit.max,
        class = {
            "Okular",
            "Evince",
            "EPDFviewer",
        },
        no_focus_stealing_out = true,
    },
    {
        name = tagnames.steam,
        init = true,
        screen = {1,2},
        position = 6,
        -- client in the "class" section will start. It will be created on
        -- the client startup screen
        exclusive = true,
        layout = awful.layout.suit.fair,
        -- exec_once = { "steam" },
        class = { "Steam" },
        instance = { "Steam" },
        no_focus_stealing_out = true,
        no_focus_stealing_in = true,
    },
    {
        name = tagnames.settings,
        init = false,
        screen = {1},
        -- client in the "class" section will start. It will be created on
        -- the client startup screen
        exclusive = true,
        layout = awful.layout.suit.fair,
        -- exec_once = { "steam" },
        class = {
            "Apper",
            "YaST2",
            "y2controlcenter",
        },
        instance = {
        },
        no_focus_stealing_out = true,
    },
    {
        name = tagnames.image_edit,
        init = false,
        screen = {1,2},
        -- client in the "class" section will start. It will be created on
        -- the client startup screen
        exclusive = true,
        layout = awful.layout.suit.fair,
        -- exec_once = { "steam" },
        class = {
            "Gimp",
            "Darktable",
            "Gwenview",
        },
        instance = {
        },
        no_focus_stealing_out = true,
        no_focus_stealing_in = true,
    },
    {
        name = "game",
        init = false,
        screen = 2,
        position = 7,
        -- client in the "class" section will start. It will be created on
        -- the client startup screen
        exclusive = true,
        layout = awful.layout.suit.magnifier,
        -- exec_once = { "steam" },
        class = {
            "StormUnited",
            "StormUnitedClient",
        },
        no_focus_stealing_out = true,
        no_focus_stealing_in = true,
    },
    {
        name = tagnames.other,
        init = true,
        screen = {1,2},
        -- client in the "class" section will start. It will be created on
        -- the client startup screen
        exclusive = false,
        fallback = true,
        layout = awful.layout.suit.fair,
        -- exec_once = { "steam" },
        class = {
            "*",
        },
        no_focus_stealing_out = true,
    },
}

-- Ignore the tag "exclusive" property for the following clients
-- (matched by classes)
tyrannical.properties.intrusive = {
    "About KDE",
    "Background color",
    "Gcr-prompter",
    "Gradient editor",
    "KDialog",
    "Knotes",
    "Kwalletd",
    "Paste Special",
    "Pavucontrol",
    "Plugin-container",
    "Xephyr",
    "feh",
    "gtksu",
    "kcalc",
    "kcolorchooser",
    "kruler",
    "ksnapshot",
    "pinentry",
    "plasmaengineexplorer",
    "sun-applet-PluginMain",
    "xcalc",
}

-- Ignore the tiled layout for the matching clients
tyrannical.properties.floating = {
    "Godot",
    "Insert Picture",
    "MPlayer",
    "New Form",
    "Paste Special",
    "Pavucontrol",
    "Plugin-container",
    "Select Color$",
    "Wine",
    "feh",
    "gtksu",
    "kcalc",
    "kcharselect",
    "kcolorchooser",
    "kmix",
    "kruler",
    "ksnapshot",
    "mythfrontend",
    "pinentry",
    "pinentry",
    "xcalc",
    "xine",
    "yakuake",
}

-- Make the matching clients (by classes) on top of the default layout
tyrannical.properties.ontop = {
    "Xephyr",
    "ksnapshot",
    "kruler",
}

-- Force the matching clients (by classes) to be centered on the screen on init
tyrannical.properties.centered = {
    "kcalc",
    "Pavucontrol",
}

tyrannical.properties.slave = {
    "Skype",
}

tyrannical.properties.fullscreen = {
    "Plugin-container",
}

-- Do not honor size hints request for those classes
tyrannical.properties.size_hints_honor = {
    xterm = false,
    URxvt = false,
    aterm = false,
    sauer_client = false,
    mythfrontend = false,
    Godot = true,
}

-- Block popups
tyrannical.settings.block_children_focus_stealing = true
-- Force popups/dialogs to have the same tags as the parent client
tyrannical.settings.group_children = true

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
local bindings = require("bindings")
local beautiful = require("beautiful")
awful.rules = require("awful.rules")
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = { },
        properties = {
            border_width = beautiful.border_width_focused,
            border_color = beautiful.border_focus,
            focus = awful.client.focus.filter,
            size_hints_honor = true,
            keys = bindings.clientkeys,
            maximized_vertical = false,
            maximized_horizontal = false,
            buttons = bindings.clientbuttons,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen,
        },
    },
    {
        rule = { class = "Dolphin" },
        except = { name = "Dolphin" },
        properties = { floating = true },
        callback = function( c )
            awful.titlebar.toggle(c)
        end
    },
    {
        rule = { class = "Knotes" },
        properties = {
            ontop = true,
            floating = true,
            skip_taskbar = false,
        }
    },
    {
        rule = { class = "Skype", name = "Call with" },
        properties = {
            ontop = true,
            floating = true,
            sticky = true,
            size_hints_honor = true,
        },
        callback = function(c)
            c:geometry({ width = 720, height = 540 })
        end
    },
}
-- }}}

-- }}}
