local awful = require("awful")
awful.mouse = require('awful.mouse')
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local bindings = require("bindings")
local config = require("config")
local gears = require("gears")
local markup = require("utils.markup")
local menu = require("menu")
local naughty = require('naughty')
local wibox = require("wibox")

require("ui.tags")

local volume = require("ui.widgets.volume")
local keymap = require("ui.widgets.keymap")
local startmenu = require("ui.widgets.startmenu")
--local netwidget = require("ui.widgets.network")
--local cpuwidget = require("ui.widgets.cpu")
mytextclock = wibox.widget {
    widget = wibox.container.place,
    halign = 'center',
    valign= 'center',
    wibox.widget.textclock("<span font_desc='DejaVu Sans Mono 14'>%a</span>%n<span font_desc='DejaVu Sans Mono 22'><small>%d%n%m%n%y</small>%n<b>%H</b>%n%M</span>"),
}

-- Create a wibar for each screen and add it
mywibar = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = gears.table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ config.modkey }, 1, function(t)
        local c = client.focus
        if not c then return end
        c:move_to_tag(t)
    end),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ config.modkey }, 3, awful.client.toggletag),
    awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
    awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end))

mytasklist = {}
mytasklist.buttons = gears.table.join(
    awful.button({ }, 1, function (c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal("request::activate", "tasklist", {raise = true})
        end
    end),
    awful.button({ }, 3, function ()
        awful.menu.client_list({theme = { width = 500 }})
    end),
    awful.button({ }, 4, function ()
        awful.client.focus.byidx(-1)
    end),
    awful.button({ }, 5, function ()
        awful.client.focus.byidx(1)
    end))

awful.screen.connect_for_each_screen(function(s)
    -- Create an imagebox widget which will contains an icon indicating
    -- which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(gears.table.join(
        awful.button({ }, 1, function() awful.layout.inc(config.layouts, 1) end),
        awful.button({ }, 3, function() awful.layout.inc(config.layouts, -1) end),
        awful.button({ }, 4, function() awful.layout.inc(config.layouts, 1) end),
        awful.button({ }, 5, function() awful.layout.inc(config.layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist {
        screen = s,
        filter = awful.widget.taglist.filter.all,
        style = {
            shape = function(cr, width, height) return gears.shape.partially_rounded_rect(cr, width, height, false, true, true, false, 6) end,
            bg_occupied = beautiful.pallette[8],
            fg_occupied = beautiful.pallette[2],
            bg_empty = beautiful.pallette[2],
            fg_empty = beautiful.pallette[1],
            bg_volatile = beautiful.pallette[11],
            fg_volatile = beautiful.pallette[10],
            bg_focus = beautiful.taglist_bg_focus,
            fg_focus = beautiful.taglist_fg_focus,
        },
        layout = {
            layout = wibox.layout.fixed.vertical,
            spacing_widget = {
                color = beautiful.bg_normal,
                shape = gears.shape.rectangle,
                widget = wibox.widget.separator,
            },
            spacing = dpi(8),
        },
        widget_template = {
            widget = wibox.container.background,
            id = 'background_role',
            {
                widget = wibox.container.margin,
                top = 8,
                bottom = 8,
                {
                    layout = wibox.layout.fixed.vertical,
                    {
                        widget = wibox.container.background,
                        bg = beautiful.pallette[8],
                        fg = beautiful.pallette[2],
                        shape = gears.shape.circle,
                        {
                            widget = wibox.widget.textbox,
                            id = 'index_role',
                            ignore_markup = true,
                            align = 'center',
                        },
                    },
                    {
                        widget = wibox.widget.textbox,
                        id = 'text_role',
                        align = 'center',
                    },
                },
            },
            -- Add support for an index label
            create_callback = function(self, t, index, tags) --luacheck: no unused args
                self:get_children_by_id('index_role')[1].markup = '<b> '..index..' </b>'
            end,
            update_callback = function(self, t, index, tags) --luacheck: no unused args
                self:get_children_by_id('index_role')[1].markup = '<b> '..index..' </b>'
            end,
        },
        buttons = mytaglist.buttons,
    }
    mytasklist[s] = awful.widget.tasklist{
        screen = s,
        filter = awful.widget.tasklist.filter.currenttags,
        buttons = mytasklist.buttons,
        layout = {
            layout  = wibox.layout.flex.horizontal,
            max_widget_size = 450,
        },
        style = {
            shape = function(cr, width, height)
                return gears.shape.partially_rounded_rect(
                cr, width, height, false, false, true, true, 12)
            end,
            bg_normal = beautiful.bg_normal,
            fg_normal = beautiful.fg_normal,
            bg_focus = beautiful.taglist_bg_focus,
            fg_focus = beautiful.taglist_fg_focus,
        },
        widget_template = {
            widget = wibox.container.background,
            id = 'background_role',
            {
                widget = wibox.container.margin,
                left = 10,
                right = 5,
                {
                    layout = wibox.layout.fixed.horizontal,
                    {
                        widget = wibox.container.margin,
                        id = 'icon_margin_role',
                        left = 6,
                        right = 6,
                        top = 3,
                        bottom = 3,
                        {
                            widget = wibox.widget.imagebox,
                            id = 'icon_role',
                        },
                    },
                    {
                        widget = wibox.widget.textbox,
                        id = 'text_role',
                    },
                },
            },
        },
    }
    -- Create the wibar
    local sep = wibox.widget {
        widget = wibox.container.margin,
        left = 12,
        right = 12,
        {
            widget = wibox.widget.separator,
            orientation = 'horizontal',
            color = beautiful.pallette[5],
            shape = gears.shape.circle,
        }
    }
    mywibar[s] = awful.wibar{
        position = "left",
        screen = s,
        width = beautiful.wibar_size,
        widget = wibox.widget {
            layout = wibox.layout.align.vertical,
            spacing_widget = sep,
            spacing = beautiful.wibar_spacing,
            {
                layout = wibox.layout.fixed.vertical,
                spacing_widget = sep,
                spacing = beautiful.wibar_spacing,

                startmenu,
                mytaglist[s],
                keymap.widget,
                {
                    layout = wibox.layout.fixed.vertical,
                    id = 'systray',
                    {
                        widget = wibox.widget.textbox,
                        markup = '<span font_desc="FontAwesome 5 Free 12"></span>',
                        align = 'center',
                        valign = 'center',
                    },
                    wibox.widget.systray(),
                },
                {
                    widget = wibox.widget.textbox,
                },
            },
            {
                widget = wibox.container.rotate,
                direction = 'east',
                mytasklist[s],
            },
            {
                layout = wibox.layout.fixed.vertical,
                spacing_widget = sep,
                spacing = beautiful.wibar_spacing,

                volume,
                mytextclock,
                mylayoutbox[s],
            },
        },
    }
    mywibar[s]:get_children_by_id('systray')[1].children[2]:set_horizontal(false)

    desktop = require('ui.desktop').render(s)
end)

-- TODO Split into own file:

local myprompt = awful.widget.prompt{with_shell = true}

local w = wibox {
    bg = beautiful.bg_normal,
    border_width = 4,
    border_color = beautiful.bg_urgent,
    ontop = true,
    height = 128,
    width = 800,
    shape = function(cr, width, height)
        gears.shape.rounded_rect(cr, width, height, 12)
    end
}

w:setup {
    layout = wibox.layout.fixed.horizontal,
    {
        layout = wibox.container.margin,
        id = 'icon',
        left = 10,
        {
            widget = wibox.widget.textbox,
            markup = '<span font_desc="FontAwesome 5 Free 32"></span>',
            align = 'center',
            valign = 'center',
        },
    },
    {
        layout = wibox.container.margin,
        left = 10,
        myprompt,
    },
}

local gfs = require("gears.filesystem")
local function launch()
    w.visible = true

    awful.placement.centered(w, { parent = awful.screen.focused()})
    awful.prompt.run{
        bg_cursor = '#84bd00',
        textbox = myprompt.widget,
        font = beautiful.font_name .. " 24",
        history_path = gfs.get_dir('cache') .. '/awesome_history',
        completion_callback = require("awful.completion").shell,
        exe_callback = function(input_text)
            if not input_text or #input_text == 0 then return end
            if input_text:find("echo", 1, true) == 1 then
                naughty.notify{
                    preset=naughty.config.presets.critical,
                    title="Echo",
                    text=input_text:sub(5, -1),
                }
                return
            end
            awful.spawn(input_text)
        end,
        done_callback = function()
            w.visible = false
        end
    }
end

bindings:bind_global(awful.key({ config.modkey, }, "r", function() launch() end))
